//
//  TableViewCell.swift
//  testTask
//
//  Created by MacMini on 30/08/17.
//  Copyright © 2017 MacMini. All rights reserved.
//

import UIKit

class TableViewCell: UITableViewCell {

    @IBOutlet var lblCollectionTitle: UILabel!
    @IBOutlet var clVw: UICollectionView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
