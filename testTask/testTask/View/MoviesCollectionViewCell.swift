//
//  MoviesCollectionViewCell.swift
//  testTask
//
//  Created by MacMini on 30/08/17.
//  Copyright © 2017 MacMini. All rights reserved.
//

import UIKit

class MoviesCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var lblMovieTitle: UILabel!
    @IBOutlet var imgMovieImage: UIImageView!
    

}
