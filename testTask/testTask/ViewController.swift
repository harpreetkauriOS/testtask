//
//  ViewController.swift
//  testTask
//
//  Created by MacMini on 30/08/17.
//  Copyright © 2017 MacMini. All rights reserved.
//

import UIKit

class ViewController: UIViewController , UICollectionViewDataSource, UICollectionViewDelegate ,UICollectionViewDelegateFlowLayout, UITableViewDelegate, UITableViewDataSource{

    @IBOutlet var tblMoviesSections: UITableView!
//    @IBOutlet var clMoviesList: UICollectionView!
     var arrNewInTheaters : NSArray! = NSArray()
    var numberCells = 0;
    var arrNowPlaying : NSArray! = NSArray()
    var arrPopular : NSArray! = NSArray()
    var arrHighlyRated : NSArray! = NSArray()

    override func viewDidLoad() {
        super.viewDidLoad()
        getPopular()
        getHighyRated()
        getNowPlaying()
        
     
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    
      // MARK:- tableview
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        
        return numberCells
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "listCell",
                                                 for: indexPath as IndexPath) as! TableViewCell
        cell.clVw.tag = indexPath.row
        if self.numberCells == 3 && indexPath.row == 0 {
            cell.lblCollectionTitle.text = "Now in Theaters"
            cell.clVw.accessibilityHint = "now"
            
        }
        else  if self.numberCells == 3 && indexPath.row == 1 {
            cell.clVw.accessibilityHint = "popular"
            cell.lblCollectionTitle.text = "Popular"
        }
        else  if self.numberCells == 3 && indexPath.row == 2 {
            cell.clVw.accessibilityHint = "high"
            cell.lblCollectionTitle.text = "Highest Rated This Year"
        }
                return cell
        
    }
    // MARK:-  collection view
    
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "header", for: indexPath as IndexPath)
        return headerView
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView.accessibilityHint == "high" {
            return self.arrHighlyRated.count
        }
        else if collectionView.accessibilityHint == "now" {
            return self.arrNowPlaying.count
        }
        else if collectionView.accessibilityHint == "popular" {
            return self.arrPopular.count
        }
        else
        {
        return 0//a.count
        }
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "moviesCell",
                                                      for: indexPath as IndexPath) as! MoviesCollectionViewCell
        
         var innerDict : NSDictionary = NSDictionary()
        if collectionView.accessibilityHint == "high" {
            innerDict = self.arrHighlyRated.object(at: indexPath.row) as! NSDictionary
            cell.lblMovieTitle.text =  innerDict.object(forKey: "title") as? String ?? ""
        }
        else if collectionView.accessibilityHint == "now" {
            innerDict = self.arrNowPlaying.object(at: indexPath.row) as! NSDictionary
            cell.lblMovieTitle.text =  innerDict.object(forKey: "title") as? String ?? ""
        }
        else if collectionView.accessibilityHint == "popular" {
            innerDict = self.arrPopular.object(at: indexPath.row) as! NSDictionary
            cell.lblMovieTitle.text =  innerDict.object(forKey: "title") as? String ?? ""
        }
        let strImage =  innerDict.object(forKey: "backdrop_path") as? String ?? ""
//        let strCompleteURL = String.init(describing: "https://image.tmdb.org/t/p/w500\(strImage)")
//        print(strCompleteURL)
        
        cell.imgMovieImage.layer.cornerRadius = 4

        downloadImageFromUrl(urlString: "https://image.tmdb.org/t/p/w500\(strImage)", imageview: cell.imgMovieImage)
       
        
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        var sz : CGSize = self.view.frame.size
        
        sz.width = (sz.width) - 20
        
        sz.height = 200;
        
        return sz;
        
    }

    func getNowPlaying() {
        
        
        let strURL: String = "https://api.themoviedb.org/3/movie/now_playing?page=1&language=en-US&api_key=425b9038ea8933bf32e3d6cdf04ccf5a"
//        print(strURL)
        
        let config = URLSessionConfiguration.default // Session Configuration
        let session = URLSession(configuration: config) // Load configuration into Session
        let url :URL = URL(string: strURL)!
        
        let task = session.dataTask(with: url, completionHandler: {
            (data, response, error) in
            
            if error != nil {
                DispatchQueue.main.async {
                }
            }
            else {
                
                do {
                    if let json: NSDictionary = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? NSDictionary
                    {
//                        print("now playing")
                        let testarr = json.object(forKey: "results") as? NSArray ?? []
                        if testarr.count > 0
                        {
                            self.arrNowPlaying = testarr
                            
                            self.numberCells = self.numberCells + 1
                            DispatchQueue.main.async {
                                self.tblMoviesSections.reloadData()
                            }
                            
                        }
                        
                    }
                    
                } catch {
                    print("error in JSONSerialization")
                    
                }
            }
        })
        task.resume()
        
    }

    func getPopular() {
        
       
        
        let strURL: String = "https://api.themoviedb.org/3/movie/popular?page=1&language=en-US&api_key=425b9038ea8933bf32e3d6cdf04ccf5a"
//        print(strURL)
        
        let config = URLSessionConfiguration.default // Session Configuration
        let session = URLSession(configuration: config) // Load configuration into Session
        let url :URL = URL(string: strURL)!
        
        let task = session.dataTask(with: url, completionHandler: {
            (data, response, error) in
            
            if error != nil {
                DispatchQueue.main.async {
                }
            }
                else {
                    
                    do {
                        if let json: NSDictionary = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? NSDictionary
                        {
//                            print("popular")
                            let testarr = json.object(forKey: "results") as? NSArray ?? []
                            if testarr.count > 0
                            {
                                self.arrPopular = testarr
                                
                                self.numberCells = self.numberCells + 1
                                DispatchQueue.main.async {
                                    self.tblMoviesSections.reloadData()
                                }
                                
                            }
                            
                        }
                        
                    } catch {
                        print("error in JSONSerialization")
                        
                    }
                }
            })
            task.resume()
        
        }
    func getHighyRated()  {
        
        
        let strURL: String = "https://api.themoviedb.org/3/movie/top_rated?page=1&language=en-US&api_key=425b9038ea8933bf32e3d6cdf04ccf5a"
//        print(strURL)
        
        let config = URLSessionConfiguration.default // Session Configuration
        let session = URLSession(configuration: config) // Load configuration into Session
        let url :URL = URL(string: strURL)!
        
        let task = session.dataTask(with: url, completionHandler: {
            (data, response, error) in
            
            if error != nil {
                DispatchQueue.main.async {
                }
            }
            else {
                
                do {
                    if let json: NSDictionary = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? NSDictionary
                    {
                        
                         let testarr = json.object(forKey: "results") as? NSArray ?? []
                        if testarr.count > 0
                        {
                            self.arrHighlyRated = testarr
                            
                            self.numberCells = self.numberCells + 1
                            DispatchQueue.main.async {
                                self.tblMoviesSections.reloadData()
                            }

                        }
//                        print("highest rated")
                                           }
                    
                } catch {
                    print("error in JSONSerialization")
                    
                }
            }
        })
        task.resume()
        
        
    }
    func downloadImageFromUrl(urlString: String, imageview: UIImageView) {
        let request = URLRequest(url: URL(string: urlString)!)
        DispatchQueue.global(qos: .default).async(execute: {() -> Void in
            
            let data: Data? = try? NSURLConnection.sendSynchronousRequest(request, returning: nil)
            
            let img = UIImage(data: data!)
            if img != nil {
                DispatchQueue.main.sync(execute: {() -> Void in
                    //profileImage=img;
                    imageview.image = img
                    imageview.clipsToBounds = true
                    imageview.layer.cornerRadius = 4

                })
            }
        })
    }
}

